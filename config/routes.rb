Rails.application.routes.draw do

  resources :patients do
    resources :gradings
  end
  
  root 'patients#index'
end
