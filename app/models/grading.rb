class Grading < ActiveRecord::Base
	belongs_to :patient
	has_many :rights, dependent: :destroy
	accepts_nested_attributes_for :rights, reject_if: :all_blank, allow_destroy: true
end
