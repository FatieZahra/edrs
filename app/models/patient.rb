class Patient < ActiveRecord::Base
	Gender = %w(Male Female)
	has_many :gradings, dependent: :destroy
	validates :first_name, :surname, :address, :phone, presence: true
	def fullName
		self.first_name+' '+self.middle_name+' '+self.surname
	end
	def gradeCount
		self.gradings.size
	end
end
