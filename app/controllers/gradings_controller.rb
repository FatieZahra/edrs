class GradingsController < ApplicationController
  before_action :set_patient
  before_action :set_grading, only: [:show, :edit, :update, :destroy]
  before_action :set_right, only:[:show]

  def index
    @gradings = Grading.all
  end

  def show
  end

  def new
    @grading = @patient.gradings.build
  end

  def edit
  end

  def create
    @grading = @patient.gradings.build(grading_params)

    respond_to do |format|
      if @grading.save
        format.html { redirect_to [@patient, @grading], notice: 'Grading was successfully created.' }
        format.json { render :show, status: :created, location: @grading }
      else
        format.html { render :new }
        format.json { render json: @grading.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @grading.update(grading_params)
        format.html { redirect_to [@patient, @grading], notice: 'Grading was successfully updated.' }
        format.json { render :show, status: :ok, location: @grading }
      else
        format.html { render :edit }
        format.json { render json: @grading.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @grading.destroy
    respond_to do |format|
      format.html { redirect_to [@patient, @grading], notice: 'Grading was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_right
      @right = @grading.rights.find(params[:id])
    end
    def set_grading
      @grading = @patient.gradings.find(params[:id])
    end

    def set_patient
      @patient = Patient.find(params[:patient_id])
    end

    def grading_params
      params.require(:grading).permit(:right_eye_verdict, :left_eye_verdict, rights_attributes: [:id, :microaneurysms, :cotton_wool_spots, :flame_haemorrhages, :irma, :venous_beading, :vessels_elsewhere, :vessels_at_disc, :vitreous_haemorrhages, :retinal_detachment, :_destroy])
    end
end
