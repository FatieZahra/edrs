json.array!(@patients) do |patient|
  json.extract! patient, :id, :first_name, :middle_name, :surname, :dob, :gender, :phone, :email, :address, :state
  json.url patient_url(patient, format: :json)
end
