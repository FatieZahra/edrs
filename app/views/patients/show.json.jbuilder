json.extract! @patient, :id, :first_name, :middle_name, :surname, :dob, :gender, :phone, :email, :address, :state, :created_at, :updated_at
