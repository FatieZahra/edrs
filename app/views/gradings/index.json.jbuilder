json.array!(@gradings) do |grading|
  json.extract! grading, :id, :right_eye_verdict, :left_eye_verdict
  json.url grading_url(grading, format: :json)
end
