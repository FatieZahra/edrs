class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :first_name
      t.string :middle_name
      t.string :surname
      t.date :dob
      t.string :gender
      t.string :phone
      t.string :email
      t.string :address
      t.string :state

      t.timestamps null: false
    end
  end
end
