class CreateGradings < ActiveRecord::Migration
  def change
    create_table :gradings do |t|
      t.string :right_eye_verdict
      t.string :left_eye_verdict

      t.timestamps null: false
    end
  end
end
