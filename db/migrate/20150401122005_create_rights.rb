class CreateRights < ActiveRecord::Migration
  def change
    create_table :rights do |t|
      t.boolean :microaneurysms
      t.boolean :flame_haemorrhages
      t.boolean :cotton_wool_spots
      t.boolean :irma
      t.boolean :venous_beading
      t.boolean :vessels_elsewhere
      t.boolean :vessels_at_disc
      t.boolean :vitreous_haemorrhages
      t.boolean :retinal_detachment
      t.belongs_to :grading, index: true

      t.timestamps null: false
    end
    add_foreign_key :rights, :gradings
  end
end
