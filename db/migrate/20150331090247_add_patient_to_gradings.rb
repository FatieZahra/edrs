class AddPatientToGradings < ActiveRecord::Migration
  def change
    add_reference :gradings, :patient, index: true
  end
end
