# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150401122005) do

  create_table "gradings", force: :cascade do |t|
    t.string   "right_eye_verdict", limit: 255
    t.string   "left_eye_verdict",  limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "patient_id",        limit: 4
  end

  add_index "gradings", ["patient_id"], name: "index_gradings_on_patient_id", using: :btree

  create_table "patients", force: :cascade do |t|
    t.string   "first_name",  limit: 255
    t.string   "middle_name", limit: 255
    t.string   "surname",     limit: 255
    t.date     "dob"
    t.string   "gender",      limit: 255
    t.string   "phone",       limit: 255
    t.string   "email",       limit: 255
    t.string   "address",     limit: 255
    t.string   "state",       limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "rights", force: :cascade do |t|
    t.boolean  "microaneurysms",        limit: 1
    t.boolean  "flame_haemorrhages",    limit: 1
    t.boolean  "cotton_wool_spots",     limit: 1
    t.boolean  "irma",                  limit: 1
    t.boolean  "venous_beading",        limit: 1
    t.boolean  "vessels_elsewhere",     limit: 1
    t.boolean  "vessels_at_disc",       limit: 1
    t.boolean  "vitreous_haemorrhages", limit: 1
    t.boolean  "retinal_detachment",    limit: 1
    t.integer  "grading_id",            limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "rights", ["grading_id"], name: "index_rights_on_grading_id", using: :btree

  add_foreign_key "rights", "gradings"
end
